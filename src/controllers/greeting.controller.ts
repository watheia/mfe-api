// Uncomment these imports to begin using these cool features!
// import {inject} from '@loopback/core';
import {get} from '@loopback/rest';
export class GreetingController {
  constructor() {}
  @get('/greet')
  hello(): string {
    return 'Hello, World!';
  }
}
